package hw3.task4;

import java.io.*;
import java.net.*;
import java.util.*;

public class RequestClient {
	private static InetAddress host;
	private static final int PORT = 12345;

	public static void main(String[] args) {
		try {

			host = InetAddress.getLocalHost();
		} catch (UnknownHostException uhEx) {
			System.out.println("\nHost ID not found!\n");
			System.exit(1);
		}
		sendMessages();
	}

	private static void sendMessages() {
		Socket socket = null;
		try {
			socket = new Socket(host, PORT);
			Scanner networkInput = new Scanner(socket.getInputStream());
			PrintWriter networkOutput = new PrintWriter(socket.getOutputStream(), true);
			// Set up stream for keyboard entry...
			Scanner userEntry = new Scanner(System.in);
			String message, response;
			do {
				System.out.print("Enter message ('0' to exit): ");
				message = userEntry.nextLine();
				if (!message.equals("1") && !message.equals("0")) {
					continue;
				}
				
				// Send message to server on the
				// socket's output stream...
				// Accept response from server on the
				// socket's intput stream...
				networkOutput.println(message);
				networkOutput.flush();
				if (networkInput.hasNext()) {
					response = networkInput.nextLine();
					// Display server's response to user...
					System.out.println("\nSERVER> " + response);
				}
			} while (!message.equals("0"));
			
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
		} finally {
			try {
				System.out.println("\nClosing connection...");
				socket.close();
			} catch (IOException ioEx) {
				System.out.println("Unable to disconnect!");
				System.exit(1);
			}
		}
	}
}