package hw3.task4;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ResponseServer {
	private static ServerSocket serverSocket;
	private static final int PORT = 12345;
	static volatile int connectionsCount = 0;
	private final static Object lock = new Object();


	public static void main(String[] args) throws IOException {
		try {
			serverSocket = new ServerSocket(PORT);
		} catch (IOException ioEx) {
			System.out.println("\nUnable to set up port!");
			System.exit(1);
		}
		
		// Create a Resource object with
		// a starting resource level of 1...
		Resource item = new Resource(1);
		// Create a Producer thread, passing a reference
		// to the Resource object as an argument to the
		// thread constructor...
		Producer producer = new Producer(item);
		// Start the Producer thread running...
		producer.start();
		
		
		do {
			// Wait for client...
			Socket client = serverSocket.accept();
			System.out.println("\nNew client accepted.\n");
			
			synchronized (lock) {
				System.out.println("Current count of connections is: " + ++connectionsCount);
			}
			
			// Create a ClientHandler thread to handle all
			// subsequent dialogue with the client, passing
			// references to both the client's socket and
			// the Resource object...
			ClientHandler handler = new ClientHandler(client, item);
			// Start the ClientThread thread running...
			handler.start();
			
		} while (true);
	}
}

class ClientHandler extends Thread {
	private Socket client;
	private Scanner input;
	private PrintWriter output;
	private Resource item;

	public ClientHandler(Socket socket, Resource resource) {
		// Set up reference to associated socket...
		client = socket;
		item = resource;
		
		try {
			// Create input and output streams
			// on the socket...
			input = new Scanner(client.getInputStream());
			output = new PrintWriter(client.getOutputStream(), true);
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
		}
	}

	public void run() {
		System.out.println("ClientHandler thread started: " + Thread.currentThread().getName());
		
		String request = "";
		do {
			// Accept message from client on
			// the socket's input stream...
			request = input.nextLine();
			System.out.println(request);
			if (request.equals("1")) {
				item.takeOne();// If none available,
				// wait until resource(s)
				// available (and thread is
				// at front of thread queue).
				output.println("Request granted.");
			}
			
			// Echo message back to client on
			// the socket's output stream...
			//output.println("ECHO: " + request);
			// Repeat above until 'QUIT' sent by client...
		} while (!request.equals("0"));
		
		try {
			if (client != null) {
				System.out.println("Closing down connection...");
				System.out.println("Current count of connections is: " + --ResponseServer.connectionsCount);
				client.close();
			}
		} catch (IOException ioEx) {
			System.out.println("Unable to disconnect!");
		}
	}
}
