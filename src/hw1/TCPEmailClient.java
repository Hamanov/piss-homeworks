package hw1;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class TCPEmailClient {
	private static InetAddress host;
	private static final int PORT = 1234;
	private static String name;
	private static Scanner networkInput, userEntry;
	private static PrintWriter networkOutput;
	private static Socket socket;

	public static void main(String[] args) throws IOException {
		try {
			host = InetAddress.getLocalHost();
		} catch (UnknownHostException uhEx) {
			System.out.println("Host ID not found!");
			System.exit(1);
		}

		userEntry = new Scanner(System.in);

		do {
			System.out.print("\nEnter name ('Dave' or 'Karen'): ");
			name = userEntry.nextLine();
		} while (!name.equals("Dave") && !name.equals("Karen"));

		while (true) {
			talkToServer();
		}
	}

	private static void talkToServer(){
		String msg = "";
		Scanner userInput = new Scanner(System.in);
		System.out.println("Please enter 'send', 'read' or 'quit'");
		msg = userInput.nextLine();
		
		try {
			socket = new Socket(host, PORT);
			networkOutput = new PrintWriter(socket.getOutputStream(), true);
			networkInput = new Scanner(socket.getInputStream());
				
			if ("send".equals(msg)) {
				doSend();
			} else if ("read".equals(msg)) {
				doRead();
			} else if ("quit".equals(msg)) {
				System.exit(1);
			}
		
		} catch (Exception e) {
			
		}
		
	}

	private static void doSend() {
		System.out.println("\nEnter 1-line message: ");
		String message = userEntry.nextLine();
		networkOutput.println(name);
		networkOutput.println("send");
		networkOutput.println(message);
	}

	private static void doRead() throws IOException {
		System.out.println("\nHere is the content of your inbox: ");
		networkOutput.println(name);
		networkOutput.println("read");
		
		String message = null;
		
		while (networkInput.hasNextLine()) {
			message = networkInput.nextLine();
			System.out.println(message);			
		}
	}
}