package hw2;

import java.io.*;

public class BranchEmpProcessor2
{
	public static void main(String[] args) {
		Employee2 emp2 = new Employee2();

		FileInputStream fIn = null;
		ObjectInputStream oIn = null;

		try {
				fIn = new FileInputStream("D:\\NewEmployee2.ser");
				oIn = new ObjectInputStream(fIn);
				emp2.readExternal(oIn);
				System.out.println("Deserialized " + emp2.fName + ", " + emp2.lName + " from NewEmployee2.ser");
				System.out.println("Deserialized employee who lives at " + emp2.address + " on " + emp2.hireDate);
				System.out.println("Deserialized salary value is " + emp2.salary);
		} catch(IOException e) {
			 	e.printStackTrace();
		} catch(ClassNotFoundException e) {
			 	e.printStackTrace();
		} finally {
			try {
				oIn.close();
				fIn.close();
			} catch(IOException e1) {
			 	e1.printStackTrace();
			}
		}
	}
} 