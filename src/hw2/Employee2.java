package hw2;

import java.io.*;
import java.util.Date;

class Employee2 implements Externalizable
{
	String lName;
	String fName;
	double salary;
	java.util.Date hireDate;
	String address;
	 
	public void writeExternal(ObjectOutput stream) throws IOException {
		stream.writeDouble(salary);
		stream.writeUTF(lName);
		stream.writeUTF(fName);
		stream.writeUTF(address);
		stream.writeObject(hireDate);
	}
	 
	public void readExternal(ObjectInput stream) throws IOException, ClassNotFoundException  {
		salary = stream.readDouble();
		lName = stream.readUTF();
		fName = stream.readUTF();
		address = stream.readUTF();
		hireDate = (Date) stream.readObject();
	}
}