package hw2;

import java.io.*;
import java.util.Date;

public class HeadQuarterEmpProcessor2
{
	public static void main(String[] args) {
		Employee2 emp2 = new Employee2();
		 
		emp2.lName = "John";
		emp2.fName = "Smith";
		emp2.salary = 50000;
		emp2.address = "12 main street";
		emp2.hireDate = new Date();
		
		FileOutputStream fOut = null;
		ObjectOutputStream oOut = null;
		 
		try {
				fOut = new FileOutputStream("D:\\NewEmployee2.ser");
				oOut = new ObjectOutputStream(fOut);
				emp2.writeExternal(oOut);
				System.out.println("An employee is serialized into D:\\NewEmployee.ser");
		} catch(IOException e) {
			 	e.printStackTrace();
		} finally {
			try {
			 	oOut.flush();
				oOut.close();
				fOut.close();
			} catch(IOException e1) {
				e1.printStackTrace();
			}
		}
	}
} 