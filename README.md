1. Inter-process Communication and Sockets Programming
2. Data Serialization
3. Multithreading
4. Concurrency Control and Synchronization
5. Remote Method Invocation
6. Multimedia Networking
7. MVC Architectures
8. PUSH Architectures
